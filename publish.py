

'''
(cd status && python -m unittest *status.py)
(rm -rf dist && python3 -m build --sdist && twine upload dist/*)
(git add --all; git commit -m "save"; git push)
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'structures/decor_pip',
	'structures/decor'
])


import nutrient._status.status_py as status_py
scan = status_py.calc ()
print (scan ["stats"])
assert (scan ["stats"] ["alarms"] == 0)
assert (scan ["stats"] ["checks"] ["alarms"] == 0)

import os
os.system ('cp structures/decor/nutrient/module.MD readme.md')
os.system ('(rm -rf dist && python3 -m build --sdist && twine upload dist/*)')